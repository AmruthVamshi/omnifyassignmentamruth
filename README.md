# Reservation Validator - Omnify Assignment

Web app that can be used as a pre-validator to check if a user(s) is/are allowed to make a reservation.

## Getting Started

Clone the project repository by running the command below if you use SSH

```bash
git clone git@bitbucket.org:AmruthVamshi/omnifyassignmentamruth.git
```

If you use https, use this instead

```bash
git clone https://AmruthVamshi@bitbucket.org/AmruthVamshi/omnifyassignmentamruth.git

```

After cloning, run:

```bash
composer install
```

```bash
npm install
```

Duplicate `.env.example` and rename it `.env`

Then run:

```bash
php artisan key:generate
```

```bash
npm run dev
```

### Prerequisites

Be sure to create your database and fill its details details in your `.env` file before running the below command:

```bash
php artisan db:seed --class=SqlSeeder
```

And finally, start the application:

```bash
php artisan serve
```

and visit [http://localhost:8000](http://localhost:8000) to see the application in action.

## Built With

* [Laravel](https://laravel.com) - The PHP Framework For Web Artisans
* [React](https://reactjs.org) - A JavaScript library for building user interfaces