<?php

namespace App\Http\Controllers\test;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DateTime;
use DateTimeZone;
use Exception;

class testController extends Controller
{

    public function test(Request $request){

        function isLeapYear($year){
            return ( (0 == $year % 4) and (0 != $year % 100) or (0 == $year % 400) )?true:false;
        }

        function findWeekday($date){
            //calculate year code
            $year_code=((int)substr($date,2,2)+floor((int)substr($date,2,2)/4))%7;
            //find month code
            $month_code_array=['01' => 0,'02' => 3,'03' => 3,'04' => 6,'05' => 1,'06' => 4,'07' => 6,'08' => 2,'09' => 5,'10' => 0,'11' => 3,'12' => 5];
            $month_code = $month_code_array[substr($date,5,2)];
            //century code is 6 since we live in 2000's
            //we could calculate this, since all of the data is in 2020 ill skip this
            $century_code = 6;
            //calculate leap code
            $leap_code = isLeapYear((int)substr($date,0,4))?(((int)substr($date,5,2)<=1)?1:0):0;
            //finally return the week day
            return ($year_code+$month_code+$century_code+(int)substr($date,8,2)-$leap_code)%7;
        }

        try {
            // getting data form request
            $req = $request->all();
            $bookingTime = $req['data']["reservation_datetime"][0];
            $user_ids = $req['data']["user_ids"];
            // getting settings data
            $settings = DB::select('select * from restriction_setting');
            $reservations_allowed = $settings[0]->n;
            $time_span = $settings[0]->d;
            $indvidual_group = $settings[0]->g;
            $time_zone = $settings[0]->tz;
            $d = new dateTime();
            //if given setting as a week we find out which day it is
            $weekday;
            if($time_span=='week'){$weekday = findWeekday(substr($bookingTime,0,10));}
            //checking
            //echo "$reservations_allowed reservations allowed per $time_span for a/an $indvidual_group in $time_zone";
            //creating an empty response object
            $res = (object)['data'=>(object)[]];
            
            $restricted_users = [];
            //logic for checking individual reservations
            if($indvidual_group=='individual'){
                //looping over all user ids
                foreach($user_ids as $user_id){

                    $reservations = DB::select("SELECT * FROM reservations where user_id = $user_id");
                    $user_booked_reservations = 0;
                    //checking if each prev reservation is in the time span given in settings
                    foreach($reservations as $reservation){

                        $prevReservationtime = $d->setTimestamp($reservation->reservation_timestamp_utc)->format('Y-m-d H:i:s');

                        if($time_span=='day' || $time_span=='month'){
                            //to slice data sting up until month or day
                            $day_or_month_no = ($time_span=='day')?10:(($time_span=='month')?7:0);
                            //checking if our prev booking time falls under day of month of current booking time
                            if(substr($bookingTime,0,$day_or_month_no)==substr($prevReservationtime,0,$day_or_month_no)){
                                $user_booked_reservations++;
                            }
                        }
                        //checking if our prev booking time falls under week of current booking time
                        elseif($time_span=='week'){
                            $endday = 6-$weekday;
                            $start_date = date("Y-m-d", strtotime("-$weekday day",strtotime($bookingTime)));
                            $end_date = date("Y-m-d", strtotime("$endday day",strtotime($bookingTime)));
                            if(($prevReservationtime>=$start_date)&&($prevReservationtime<=$end_date)){
                                $user_booked_reservations++;
                            }
                        }
                        // If there was any error while specifing time span 
                        else{
                            throw new Exception('Ivalid time span - it should be week, month or day');
                        }

                    }
                    //checking if prev reservations count falls under the settings criteria
                    if($user_booked_reservations>=$reservations_allowed){
                        $restricted_users[] = $user_id;
                    }
                }

            }
            
            elseif($indvidual_group=='group'){
                $reservations = DB::select("SELECT * FROM reservations where user_id = $user_ids[0]");

                foreach($reservations as $reservation){
                    $prevReservationtimestamp = $reservation->reservation_timestamp_utc;
                    $prevReservationtime = $d->setTimestamp($reservation->reservation_timestamp_utc)->format('Y-m-d H:i:s');
                    $group_booked_reservations=0;
                    $is_in_time_span = false;

                    if($time_span=='day' || $time_span=='month'){
                        $day_or_month_no = ($time_span=='day')?10:(($time_span=='month')?7:0);
                        if(substr($bookingTime,0,$day_or_month_no)==substr($prevReservationtime,0,$day_or_month_no)){
                            $is_in_time_span = true;
                        }
                    }
                    
                    elseif($time_span=='week'){
                        $endday = 6-$weekday;
                        $start_date = date("Y-m-d", strtotime("-$weekday day",strtotime($bookingTime)));
                        $end_date = date("Y-m-d", strtotime("$endday day",strtotime($bookingTime)));
                        if(($prevReservationtime>=$start_date)&&($prevReservationtime<=$end_date)){
                            $is_in_time_span = true;
                        }
                    }

                    //I know the time complexity of below algorithm is very bad :(
                    if($is_in_time_span){
                        $did_reservation_matched=false;
                        for($user_id=1;$user_id<count($user_ids);$user_id++){
                            $matched_reservations = DB::select("SELECT * FROM reservations where user_id = $user_ids[$user_id] and reservation_timestamp_utc = $prevReservationtime");
                            if(count($matched_reservations)!=0){$did_reservation_matched=true;}
                            else{$did_reservation_matched=false;break;}
                        }
                        if($did_reservation_matched){
                            $group_booked_reservations++;
                        }
                    }

                }
                
                if($group_booked_reservations>=$reservations_allowed){
                    $restricted_users = $user_ids;
                }

            }
            
            else{
                throw new Exception('Please mention group or individual'); 
            }
            // formatting the response object
            $res->data->is_booking_restricted=count($restricted_users)!=0?true:false;
            $res->data->restricted_user_ids=$restricted_users;

            return response()->json($res,200);
        }
        catch (exception $e) {
            return response()->json([
                'Message'=>'Some error occured',
                'Error'=>$e->getMessage()
            ],203);
        }
    }
    
}
