<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
Use DB;

class SqlSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path = base_path() . '/database/seeds/reservations.sql';
        $sql = file_get_contents($path);
        DB::unprepared($sql);

        $path_settings = base_path() . '/database/seeds/reservation_setting.sql';
        $sql_settings = file_get_contents($path_settings);
        DB::unprepared($sql_settings);
    }
}
