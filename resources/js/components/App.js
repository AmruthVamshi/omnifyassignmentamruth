import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route } from "react-router-dom";
import Settings from './Settings.js';
import Reservation from './Reservation.js';

function App() {
    return (
        <div className="App">
			<BrowserRouter>
				<Route exact path = "/" component = {Settings} />
				<Route path = "/reservation" component = {Reservation} />
			</BrowserRouter>
		</div>
    );
}

ReactDOM.render(<App />, document.getElementById('index'));

