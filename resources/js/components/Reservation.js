import React, { useState } from 'react';
import '../../css/Reservation.css';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import axios from 'axios';
import swal from 'sweetalert';


const Reservation = ()=>{
    const [date, setDate] = useState(new Date());
    const [userIds,setUserIds] = useState('');

    const bookingHandler=()=>{
        try {
            //formatting userIDs
            let userIdArray = userIds.split(',');
            userIdArray = userIdArray.map((id)=>{
                let i = parseInt(id);
                if(i+''==="NaN") throw 'enter a valid number as ID';
                return i;
            });
            
            //formatting date
            let dateString = date.toISOString().split('T');
            dateString = `${dateString[0]} ${dateString[1].substring(0,8)}`;
            
            var data = JSON.stringify({
                "data":{
                    "user_ids":userIdArray,
                    "reservation_datetime":[dateString]
                }
            });

            var config = {
                method: 'post',
                url: '/api/checkreservation',
                headers: { 
                    'Content-Type': 'application/json'
                },
                data : data
            };

            axios(config)
            .then(function (response) {
                if(response.status==200){
                    if(response.data.data.is_booking_restricted){
                        swal("Booking restricted",`Restricted ID's: ${JSON.stringify(response.data.data.restricted_user_ids)}`, "error");
                    }else{
                        swal("Success","Your booking is complete",'success');
                    }
                }
            })
            .catch(function (error) {
                console.log(error);
            });


        } catch (error) {
            console.log(error)
        }
        
    }

    return (
        <div className='reservation'>
            <div className='innerContainer'>
                <div className='reservationForm'>
                    <h1 className='head'>Book your reservation</h1>
                    <div className='reFormBox'>
                        <div className='reform'>
                            <input className='userIdInput' value={userIds} onChange={(e)=>{setUserIds(e.target.value)}} placeholder="User Id's eg- 1,2"/>
                            <DatePicker 
                                className='userIdInput' 
                                showTimeSelect 
                                showTimeInput
                                selected={date}
                                onChange={date => setDate(date)} 
                            />
                        </div>
                        <button onClick={bookingHandler} className='setButton'>Book</button>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Reservation;