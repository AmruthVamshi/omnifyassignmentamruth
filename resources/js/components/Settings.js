import React, { useState } from 'react';
import '../../css/Settings.css';
import Dropdown from 'react-dropdown';
import 'react-dropdown/style.css';
import axios from 'axios';
import qs from 'qs';

const Settings = (props)=>{
    const [Reservations,setReservations] = useState('2');
    const [TimeSpan,setTimeSpan] = useState('day');
    const [Individual,setIndividual] = useState('individual');
    const [TimeZone,setTimeZone] = useState('UTC');

    const optionsTimeSpan = [
        'day','week','month'
    ];
    const optionsIndividual = [
        'individual','group'
    ];
    const optionsTimeZone = [
        'UTC','asia/Kolkata'
    ];

    const onSelectTimeSpan=(option)=>{
        setTimeSpan(option.label)
    }

    const onSelectIndividual=(option)=>{
        setIndividual(option.label)
    }

    const onSelectTimeZone=(option)=>{
        setTimeZone(option.label)
    }
    
    const settingsHandler = ()=>{
        console.log(`${Reservations} reservations per ${TimeSpan} for a/an ${Individual} at ${TimeZone} timezone.`);

        var data = qs.stringify({
            'n': Reservations,
            'd': TimeSpan,
            'g': Individual,
            'tz': TimeZone 
        });

        var config = {
            method: 'post',
            url: '/api/changesettings',
            headers: { 
            'Content-Type': 'application/x-www-form-urlencoded'
            },
            data : data
        };
        
        axios(config)
        .then(function (response) {
            if(response.status==200){
                props.history.push('/reservation');
            }
        })
        .catch(function (error) {
            console.log(error);
            alert('some error occured! :(');
        });
        
    }

    return(
        <div className='settings'>
            <div className='innerContainer'>
                <div className='settingsForm'>
                    <h1 className='head'>Set your reservation settings</h1>
                    <div className='formBox'>
                        <div className='form'>
                                Allow <input className = 'reservationInput' value={Reservations} onChange={(e)=>{setReservations(e.target.value)}} placeholder='no of reservations' />
                                reservations per 
                                <Dropdown options={optionsTimeSpan} onChange={onSelectTimeSpan} value={TimeSpan} placeholder="No of reservations" />
                                for a/an 
                                <Dropdown options={optionsIndividual} onChange={onSelectIndividual} value={Individual} placeholder="No of reservations" /> 
                                in 
                                <Dropdown options={optionsTimeZone} onChange={onSelectTimeZone} value={TimeZone} placeholder="No of reservations" />
                                timezone
                        </div>
                        <button className='setButton' onClick={settingsHandler}>Set Settings</button>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Settings;