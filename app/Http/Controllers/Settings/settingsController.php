<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use Exception;

class settingsController extends Controller
{
    public function changeSettings(Request $request){
        try {
            $req_data = $request->all();
            $n = (int)$req_data['n'];
            $d = $req_data['d'];
            $g = $req_data['g'];
            $tz = $req_data['tz'];
            $id=1;
            DB::update('update restriction_setting set n = ?,d=?,g=?,tz=? where id = ?',[$n,$d,$g,$tz,$id]);
            return response()->json('sucessfully updated settings',200);
        } catch (exception $e) {
            return response()->json([
                'Message'=>'Some error occured',
                'Error'=>$e->getMessage()
            ],203);
        }
    }
}
